################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Steppers/src/main.cpp 

OBJS += \
./Steppers/src/main.o 

CPP_DEPS += \
./Steppers/src/main.d 


# Each subdirectory must supply rules for building sources it contributes
Steppers/src/%.o Steppers/src/%.su: ../Steppers/src/%.cpp Steppers/src/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F303x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/krist/STM32CubeIDE/workspace_1.10.1/Stppers/Steppers_Final/Steppers/inc" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -specs=rdimon.specs -lc -lrdimon -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Steppers-2f-src

clean-Steppers-2f-src:
	-$(RM) ./Steppers/src/main.d ./Steppers/src/main.o ./Steppers/src/main.su

.PHONY: clean-Steppers-2f-src

