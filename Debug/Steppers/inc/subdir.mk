################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Steppers/inc/Steppers.cpp 

OBJS += \
./Steppers/inc/Steppers.o 

CPP_DEPS += \
./Steppers/inc/Steppers.d 


# Each subdirectory must supply rules for building sources it contributes
Steppers/inc/%.o Steppers/inc/%.su: ../Steppers/inc/%.cpp Steppers/inc/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F303x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Steppers-2f-inc

clean-Steppers-2f-inc:
	-$(RM) ./Steppers/inc/Steppers.d ./Steppers/inc/Steppers.o ./Steppers/inc/Steppers.su

.PHONY: clean-Steppers-2f-inc

