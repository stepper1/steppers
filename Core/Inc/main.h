/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern void initialise_monitor_handles(void);
void APP_Main();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOF
#define TEST_Pin GPIO_PIN_0
#define TEST_GPIO_Port GPIOA
#define FIRST_FR_Pin GPIO_PIN_1
#define FIRST_FR_GPIO_Port GPIOA
#define VCP_RX_Pin GPIO_PIN_3
#define VCP_RX_GPIO_Port GPIOA
#define SECOND_BC_Pin GPIO_PIN_4
#define SECOND_BC_GPIO_Port GPIOA
#define SECOND_FR_Pin GPIO_PIN_6
#define SECOND_FR_GPIO_Port GPIOA
#define THIRD_BC_Pin GPIO_PIN_7
#define THIRD_BC_GPIO_Port GPIOA
#define BLUE_Pin GPIO_PIN_0
#define BLUE_GPIO_Port GPIOB
#define YELLOW_Pin GPIO_PIN_1
#define YELLOW_GPIO_Port GPIOB
#define THIRD_FR_Pin GPIO_PIN_12
#define THIRD_FR_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define FIRST_BC_Pin GPIO_PIN_3
#define FIRST_BC_GPIO_Port GPIOB
#define RED_Pin GPIO_PIN_6
#define RED_GPIO_Port GPIOB
#define GREEN_Pin GPIO_PIN_7
#define GREEN_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
