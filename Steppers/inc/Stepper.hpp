/*
 * Stepper.hpp
 *
 *  Created on: Jun 22, 2022
 *      Author: kristerkaldre
 */
#include "main.h"
#include <string.h>
#include <stdio.h>

#ifndef INC_STEPPER_HPP_
#define INC_STEPPER_HPP_

class Steppers
{

private:
	GPIO_TypeDef* dir_port ;
	uint16_t dir_pin;

	GPIO_TypeDef* step_port;
	uint16_t step_pin;

	TIM_HandleTypeDef* timer;
	uint32_t channel;


public:
	int64_t steps = 0;
	int64_t steps_to_do = 0;
	uint8_t dir = 0;


	//Configuring the stepper steps and directions pins
	Steppers(GPIO_TypeDef* new_dir_port, uint16_t new_dir_pin,GPIO_TypeDef* new_step_port, uint16_t new_step_pin, TIM_HandleTypeDef* new_timer, uint32_t new_channel)
	{
		dir_port = new_dir_port;
		dir_pin = new_dir_pin;


		step_port = new_step_port;
		step_pin = new_step_pin;

		timer = new_timer;
		channel = new_channel;

	}

	void Work(int64_t step_cnt, uint8_t direction)
	{

		steps_to_do = step_cnt;

		HAL_GPIO_WritePin(dir_port, dir_pin, (GPIO_PinState) direction);

		//Starts the PWM, does only one cycle
		HAL_TIM_PWM_Start_IT(timer, channel);

	}

	void InterruptHandle(void)
	{
		HAL_GPIO_ReadPin(dir_port, dir_pin) ? steps++ : steps--;
		steps_to_do--;

		//checking the numbers of steps to make
		if (steps_to_do > 0)
		{
			/*Starts the PWM signal generation in interrupt mode*/
			HAL_TIM_PWM_Start_IT(timer, channel);
		} else {
			HAL_TIM_PWM_Stop_IT(timer, channel);
		}
	}

};


#endif /* INC_STEPPER_HPP_ */
