/*
 * main.cpp
 *
 *  Created on: Jun 22, 2022
 *      Author: kristerkaldre
 */
#include "main.h"
#include "tim.h"
#include "gpio.h"
#include "Stepper.hpp"
#include "usart.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/*ASCII table characters*/
#define BIG_LETTER_S		83		//ASCIi Code for S
#define SMALL_LETTER_S		115		//ASCII code for s
#define SMALL_LETTER_A		97		//ASCII code for a
#define	SMALL_LETTER_H		104		//ASCII code for h
#define NUMBER_0			49		//ASCII code for 0
#define	NUMBER_8     		56		//ASCII code for 8



/*Private structures*/

/*Global variables for Tx and Rx*/
uint8_t data[7] = {0};
uint8_t i = 0;
uint8_t	a = 0;
uint8_t checksum = 0;

//uint8_t counter = 0;








/*Global variables for Stepper*/
Steppers stepper_a(FIRST_BC_GPIO_Port, FIRST_BC_Pin, FIRST_FR_GPIO_Port, FIRST_FR_Pin, &htim2, TIM_CHANNEL_2);
Steppers stepper_b(SECOND_BC_GPIO_Port,SECOND_BC_Pin,SECOND_FR_GPIO_Port,SECOND_FR_Pin, &htim3, TIM_CHANNEL_1);
Steppers stepper_c(THIRD_BC_GPIO_Port,THIRD_BC_Pin,THIRD_FR_GPIO_Port,THIRD_FR_Pin, &htim16, TIM_CHANNEL_1);

/*
void Test()
{
	HAL_GPIO_WritePin(FIRST_BC_GPIO_Port, FIRST_BC_Pin, GPIO_PIN_SET);
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	//__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2,128);

	while(1)
	{
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2,128);
	}
}
*/
void APP_Main()
{

	//Test();
	//HAL_GPIO_WritePin(FIRST_BC_GPIO_Port, FIRST_BC_Pin, GPIO_PIN_SET);
	//HAL_GPIO_WritePin(TEST_GPIO_Port, TEST_Pin, GPIO_PIN_SET);

	/*Starting the timers in interrupt mode */
	HAL_TIM_Base_Start_IT(&htim2);
	HAL_TIM_Base_Start_IT(&htim3);
	HAL_TIM_Base_Start_IT(&htim16);



	/*Enabling the duty cycle at 50%*/
	__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2,128);
	__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,128);
	__HAL_TIM_SET_COMPARE(&htim16,TIM_CHANNEL_1,128);


	/*Receives the first byte*/
	//HAL_UART_Receive_IT(&huart2, data, 1);
	HAL_UART_Receive_IT(&huart1, data, 1);

	HAL_GPIO_WritePin(TEST_GPIO_Port, TEST_Pin, GPIO_PIN_SET);

	stepper_a.Work(200,1);
	while(1)
	{
		//stepper_a.Work(150, 1);

	}



	/*while(stepper_a.steps_to_do != 0);
	HAL_Delay(1000);
	stepper_a.Work(30,0);
	while(stepper_a.steps_to_do != 0);
	HAL_Delay(1000);
	stepper_b.Work(309, 1);
	stepper_c.Work(450, 1);*/




	while(true);


}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{

	/*Checking the Pin state
	 * 0 - counter clockwise
	 * 1 - clockwise*/
	if(htim == &htim2)
	{

		stepper_a.InterruptHandle();

	}
	else if(htim == &htim3)
	{
		stepper_b.InterruptHandle();

	}
	else if(htim == &htim16)
	{
		stepper_c.InterruptHandle();
	}

}

/*Rx weak function complete callback*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	//memset(data + 1,0,sizeof(data) - 1);
	/*checking of the first letter is*/
	if((BIG_LETTER_S == data[0] || SMALL_LETTER_S == data[0]))
	{
		/*Green LED indicator of the start byte*/
		HAL_GPIO_TogglePin(GREEN_GPIO_Port, GREEN_Pin);
		/*Receive the rest of the bytes*/
		HAL_UART_Receive(huart, data + 1, 6,HAL_MAX_DELAY);

	}
	else
	{
		HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
		HAL_UART_Receive_IT(huart, data, 1);
		return;
		//Error_Handler();
	}

	/*Checks the parameters of the message*/
	for(i = 1; i < sizeof(data)-1 ; i ++)
	{
		/*is it a digit ?*/
		if(isalpha(data[i]))
		{
			data[i] = tolower(data[i]);
		}
		if(data[i] >= SMALL_LETTER_A && data[i] <= SMALL_LETTER_H && isalpha(data[i]))
		{
			HAL_GPIO_TogglePin(BLUE_GPIO_Port, BLUE_Pin);
			//HAL_Delay(250);

		}
		else if(data[i] >= NUMBER_0 && data[i] <= NUMBER_8 && isdigit(data[i]))
		{
			HAL_GPIO_TogglePin(YELLOW_GPIO_Port, BLUE_Pin);
			//HAL_Delay(250);
		}
		else
		{
			/*if non of the above is true indication is LED color red*/
			HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
			break;
		}
	}

	/*Checksum*/
	for(i = 0; i < sizeof(data) - 1 ;i++)
	{
		checksum += data[i];
	}

	if (data[6] != checksum)
	{
		HAL_GPIO_TogglePin(BLUE_GPIO_Port, BLUE_Pin);
		HAL_Delay(250);
	}

	//data[counter + 1] = checksum;
	HAL_UART_Transmit(huart, &checksum, 1, 100);
	/*Wait for the first byte*/
	HAL_UART_Receive_IT(huart, data, 1);

}


